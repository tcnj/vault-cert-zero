vault {
    address = "http://127.0.0.1:8200"
}

auto_auth {
    method "approle" {
        config = {
            role_id_file_path = "/vault/agent/role_id.txt"
        }
    }
}

template {
    source      = "/etc/nginx/vault.crt.ctmpl"
    destination = "/etc/nginx/tls/vault.crt"
}

template {
    source      = "/etc/nginx/vault.key.ctmpl"
    destination = "/etc/nginx/tls/vault.key"
}

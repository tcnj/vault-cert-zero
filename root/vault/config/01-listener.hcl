listener "tcp" {
    address = "127.0.0.1:8200"
    tls_disable = "true"
    x_forwarded_for_authorized_addrs = "127.0.0.1/32"
}

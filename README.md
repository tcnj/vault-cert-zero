# vault-cert-zero

This is the source for a docker image that aims to solve the "certificate zero" problem with HashiCorp Vault.

The basic idea of this container is to have vault self-issue its TLS certificates. The simplest way for this to work is actually to have vault listen only on localhost, and then have an NGINX proxy that performs TLS termination (no HTTP only though) and listens publicly. Apart from Vault and NGINX, there are two other components: Vault Agent (to issue the certificates from Vault) and an initialisation script that sets up an AppRole, a PKI mount and a policy for the Agent to issue its certificates from. As this requires vault to be initialised and unsealed, the script initialises vault with a single keyshare, unseals vault, performs its actions with the generated root token, then rekeys vault as configured by environment variables and regenerates a new root token (revoking the old one).

### Notable paths

#### `/vault/data`

*This should be mounted as a volume in most configurations*

This is where vault is configured to store its data.

#### `/etc/nginx/ssl`

*This should be mounted as a volume in most configurations*

This folder contains the current SSL certificate and key used by NGINX. It is recommended to mount this as a volume so that vault can be accessed and unsealed on boots subsequent to the first, as new certificates cannot be issued until vault is unsealed.

#### `/vault/config`

This is where vault's config is stored. This shouldn't need changing under most circumstances, however if audit logging/telemetry is desired, you may store extra configuration in files named following the form `XX-foobar.ext` where `XX` is a two digit number >= 10, `foobar` is a descriptive name and `ext` is either `json` or `hcl`.

#### `/vault/bootstrap`

*Operator access to this should be planned upon first run, potentially by mounting as a volume*

This is the folder in which the initialisation script will place various pieces of important data for the operator the first time the container is run. This includes:

* `ca.pem`: A PEM-formatted file containing the public portion of the generated CA certificate
* `root_token.json`: the JSON response from vault containing the root token
* `seal_keys.json`: the JSON response from vault containing the (possibly PGP encrypted) seal keys
* `.docker-cert-zero-done`: written on every boot once the init script has determined it is finished

With the exception of `.docker-cert-zero-done`, none of these files are written on boots subsequent to the one in which vault is initialised. Furthermore, none of these files are used or relied upon by any of the container's inner workings, and may be managed/deleted at the operator's discretion.

### Environment variables

|Name|Needed after first boot?|Default value|Description|
|-|-|-|-|
|TLS_DOMAINS|Yes|`localhost`|A comma-separated list of domains to put on the TLS certificate. The role at `vault_tls_pki/roles/vault_tls_listener` needs reconfiguring manually if this is changed after the first boot.|
|TLS_CN|Yes|`localhost`|The domain to be used as the Common Name of the TLS certificate. Must be included in `TLS_DOMAINS`, but can be swapped at any time.|
|SEAL_PGP_KEYS|No|-|A value for the `-pgp-keys` option when rekeying vault. The option will be excluded if the variable is not present. See the [vault docs](https://www.vaultproject.io/docs/commands/operator/rekey#pgp-keys) for more info.|
|SEAL_KEY_SHARES|No|1|The total number of keyshares to rekey vault with.|
|SEAL_KEY_THRESHOLD|No|1|The minimum number of keyshares required to unseal vault.|

### License

The full contents of this repository is licensed under the MIT open source license. See the LICENSE.md file in the root of the repository for more information including the full terms of the license.

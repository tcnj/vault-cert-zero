FROM nginx:latest

ARG S6_OVERLAY_VERSION=2.1.0.0

ARG VAULT_VERSION=1.5.4
# Valid values are amd64, 386, arm, arm64
ARG VAULT_ARCH=amd64

RUN addgroup vault && \
    adduser --system --ingroup vault vault

ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-amd64.tar.gz /tmp/
RUN tar xzf /tmp/s6-overlay-amd64.tar.gz -C / && tar xzf /tmp/s6-overlay-amd64.tar.gz -C /usr ./bin

RUN apt-get update && apt-get install -y wget unzip jq && \
    wget -O vault.zip https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_${VAULT_ARCH}.zip && \
    unzip vault.zip -d /usr/local/bin && rm vault.zip && rm -rf /var/lib/apt/lists/* && \
    mkdir -p /vault/config /vault/data

COPY root /

ENTRYPOINT [ "/init" ]

CMD [ "vault", "server", "-config", "/vault/config/" ]
